package com.Assignment;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;


public class PutMethod {
	String accessToken ="Bearer a032ff2b81e1e8ab1f8fc6e2907f88646f16c30ec2c2499fc630615530f174b7";
	String id = "";
	JSONObject req = new JSONObject();
	String baseURI = "https://gorest.co.in";

	@Test
	public void putMethod() {
		req.put("name", "Saikumar ravi");
		req.put("email", "ravisaikumar97@gmail.com");
		req.put("gender", "male");
		req.put("status", "active");
		
		String put = given().baseUri(baseURI).header("Authorization",accessToken)
				.body(req.toJSONString())
				.when().put("/public/v2/users/"+id)
				.then().extract().response().asString();
		System.out.println(put);
		req.clear();
	}}