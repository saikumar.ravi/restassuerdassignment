package com.Assignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class PostMethod {
	String accessToken ="Bearer a032ff2b81e1e8ab1f8fc6e2907f88646f16c30ec2c2499fc630615530f174b7";
	String id = "";
	JSONObject req = new JSONObject();
	String baseURI = "https://gorest.co.in";
	@Test(priority = 1)
	public void postMethod() {
		req.put("name", "Saikumar");
		req.put("email", "ravisaikumar97@gmail.com");
		req.put("gender", "male");
		req.put("status", "active");
		String post = given().baseUri(baseURI).headers("Authorization",accessToken,"Content-Type","application/json")
				.body(req.toJSONString())
				.when().post("/public/v2/users")
				.then().extract().response().asString();
		JsonPath jsp = new JsonPath(post);
		id = jsp.getString("id");
		System.out.println(post);
		req.clear();
	}
}