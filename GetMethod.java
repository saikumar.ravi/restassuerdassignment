package com.Assignment;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetMethod {
  @Test
  
	  public void getStatuscode() {
		  Response response=RestAssured.get("https://gorest.co.in/public/v2/users");
		  int statuscode=response.getStatusCode();
		  System.out.println(statuscode);
	  }
  @Test
  public void assertion() {
	  Response response=RestAssured.get("https://gorest.co.in/public/v2/users");
	  int statuscode=response.getStatusCode();
	  Assert.assertEquals(200,statuscode);
	  
  }
  }

